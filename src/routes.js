const express = require('express')
const { celebrate, Segments, Joi } = require('celebrate')
// references: 
// https://www.npmjs.com/package/celebrate
// https://hapi.dev/tutorials/?lang=en_US

const OngController = require('./controllers/OngController')
const IncidentController = require('./controllers/IncidentController')
const ProfileController = require('./controllers/ProfileController')
const SessionsController = require('./controllers/SessionsController')

const routes = express.Router()
// TODO: verificação com o celebrate nas demais rotas
routes.post('/sessions', SessionsController.login)

routes.get('/ongs', OngController.list)
routes.post('/ongs', celebrate({
  [Segments.BODY]: Joi.object().keys({
    name: Joi.string().required(),
    email: Joi.string().required().email(),
    whatsapp: Joi.string().required().min(10).max(13),
    city: Joi.string().required(),
    uf: Joi.string().required().length(2),
  })
}), OngController.create)

routes.get('/profile', celebrate({
  [Segments.HEADERS]: Joi.object({
    authorization: Joi.string().required(),
  }).unknown()
}), ProfileController.listByOng)

routes.get('/incidents', celebrate({
  [Segments.QUERY]: Joi.object().keys({
    page: Joi.number(),     
  })
}), IncidentController.list)
routes.post('/incidents', IncidentController.create)
routes.delete('/incidents/:id', celebrate({
  [Segments.PARAMS]: Joi.object().keys({
    id: Joi.number().required(),
  }),
  [Segments.HEADERS]: Joi.object({
    authorization: Joi.string().required(),
  }).unknown()
}), IncidentController.delete)

module.exports = routes
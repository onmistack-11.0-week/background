const express = require('express')
const cors = require('cors')
const { errors } = require('celebrate')
const routes = require('./routes')

const app = express()

app.use(cors())
app.use(express.json()) // para as requisições serem convertidas para JSON
app.use(routes)

app.use(errors())

module.exports = app

/**

* Tipo de parâmetros:

* Query: Parâmetros nomeados enviados na rota após o símbolo de interrogação (filtros, paginação)
* ex: ?aula=2&tipo=react&aluno=Giovanni

* Route Params: Parâmetros utilizados para identificar recursos
ex: /user/:id -> /user/1

* Request Body: Corpa da requisição, utilizado para alterar ou criar recursos (PUT, POST)

**/

/**

* SQL: MySQL, SQLite, PostgreSQL, Oracle
    - Driver: SELECT * FROM users
    - Query Builder: table('users').select('*').where() (Knex.js - http://knexjs.org/)
* NoSQL: MongoDB, CouchDB

**/

/** 

Após o knex instalado, para inicializar as configurações do banco de dados...

1- Rode o comando: 'knex init' para criar o arquivo knexfile.js

2- Após, altere o development > connect > filename para o arquivo principal do bd
    Ex: './src/database/db.sqlite'
obs: é altamente recomendável criar uma pasta 'database' com a pasta 'migrations' dentro

3- Executar o comnando 'npx knex migrate:make [migration name]' para criar um arquivo básico com as migrações
obs: dentro do arquivo criado, terá a função 'up' (no momento da criação da tabela) e o 
'down' (caso tenha problemas e precise voltar atrás), normalmente deletando a própria tabela

4- Terminando a configuração das funções 'up' e 'down', execute 'npx knex migrate:latest' para executar o último
arquivo de migrations criado. 
obs¹: com o final do processo, o arquivo nomeado no passo 2 deverá ser criado.
obs²: caso necessite criar uma migration sem ser a configurado no último artigo, execute:
    - npx knex migrate:up [migration name]
obs³: caso criação da migration esteja errada e precise executar o 'down', pode usar os seguintes comandos:
    - npx knex migrate:rollback
    - npx knex migrate:down [migration name]
obs4: para acessar os status das migrations, execute o comando:
    - npx knex migrate:status

**/

/** 

* CORS:

* instalando o cors: 'npm install cors'

* configurar: 
    - dev: app.use(cors())
    - prod: app.use(cors({ origin: [endereço que vai poder acessar]}))

 **/

 /** 

* DEPLOY:

reference
https://www.youtube.com/watch?v=CCHQpkc10s4

* Heroku (https://dashboard.heroku.com/) - Aplicações Simples
* reference: https://www.youtube.com/watch?v=-j7vLmBMsEU

* Digital Ocean (https://www.digitalocean.com/) - Aplicações Maiores
* reference: https://www.youtube.com/watch?v=ICIz5dE3Xfg

*** Outras opções:
* AWS (Amazon), Microsoft Asure, Google Cloud Plataform

**/

/** 

* TODO: (Study) Próximos estudos:
* 1- Autentificação JWT

**/
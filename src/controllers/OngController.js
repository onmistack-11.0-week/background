const connection = require('../database/connection')

const generateUniqueId = require('../utils/generateUniqueId')

module.exports = {
  async create(request, response) {
    const { name, email, whatsapp, city, uf } = request.body
    const id = generateUniqueId()

    await new Promise((resolve, reject) => {
      connection('ongs').insert({
        id,
        name,
        email,
        whatsapp,
        city,
        uf,
    })
      .then(data => resolve(data))
      .catch(error => reject(error))
    })

    return response.json({ id })
  },

  async list(request, response) {
    const ongs = await connection('ongs').select('*')
    return response.json(ongs)
  }
}